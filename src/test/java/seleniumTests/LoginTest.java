package seleniumTests;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoginTest extends BaseForTests {

    @Test
    void successfulLogin(){
        login("Chef", "mdp");
        assertTrue(driver.getTitle().contains("Page des TER de M1"));
        driver.close();
    }

    @Test
    void passwordError(){
        login("Chef", "erreur");
        WebElement  error= driver.findElement(By.className("alert-danger"));
        String errorText=read(error);
        assertTrue(errorText.contains("Bad credentials"));
        driver.close();
    }

    @Test
    void scenario1test(){
        login("Lovelace", "lovelace");
        driver.findElement(By.linkText("Teachers List")).click();
        driver.findElement(By.linkText("Add Teachers")).click();
        assertTrue(driver.getTitle().contains("Error"));
        driver.close();
    }

    @Test
    void scenario2() throws IOException {
        login("Chef", "mdp");
        driver.findElement(By.linkText("Teachers List")).click();
        driver.findElement(By.linkText("Add Teachers")).click();
        write(driver.findElement(By.id("firstName")),"TeacherfirstnameforTest");
        write(driver.findElement(By.id("lastName")),"TeacherLastNameForTest");
        WebElement createButton=driver.findElement(By.cssSelector("[type=submit]"));
        createButton.click();
        WebElement body=driver.findElement(By.tagName("body"));
        assertTrue(body.getText().contains("TeacherfirstnameforTest"));
        assertTrue(body.getText().contains("TeacherLastNameForTest"));
        screenshot("screen");
        driver.close();
    }
    // On note en passant que l'authentification, en cas de login erroné ce serait un test à inclure ici),
    // affiche une vilaine exception ce qui n'est ni très sérieux ni très raisonnable,
    // il faudrait également renvoyer un bad credentials ...
}