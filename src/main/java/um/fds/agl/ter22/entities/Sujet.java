package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;
@Entity
public class Sujet {

    private String enseignantResponsable;
    private String titre;
    private @Id @GeneratedValue Long id;
    private String encadrant;

    private String groupeEtu="";

    public Sujet(String enseignantResponsable, String titre, String encadrant){
        this.enseignantResponsable=enseignantResponsable;
        this.titre=titre;
        this.encadrant=encadrant;
    }

    public Sujet() {

    }

    public String getEncadrant() {
        return this.encadrant;
    }

    public void setEncadrant(String encadrant) {
        this.encadrant = encadrant;
    }

    public String getEnseignantResponsable() {
        return enseignantResponsable;
    }

    public void setEnseignantResponsable(String enseignantResponsable) {
        this.enseignantResponsable = enseignantResponsable;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Long getId() {
        return id;
    }

    public String getGroupeEtu() {
        return groupeEtu;
    }

    public void setGroupeEtu(String groupeEtu) {
        this.groupeEtu = groupeEtu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sujet sujet = (Sujet) o;
        return Objects.equals(enseignantResponsable, sujet.enseignantResponsable) && Objects.equals(titre, sujet.titre) && Objects.equals(id, sujet.id) && Objects.equals(encadrant, sujet.encadrant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(enseignantResponsable, titre, id, encadrant);
    }


}
