package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Groupe;


public interface GroupeRepository <T extends Groupe> extends CrudRepository<T, Long> {


    @PreAuthorize("hasRole('ROLE_MANAGER')")
    Groupe save(@Param("groupe") Groupe groupe);


    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long id);


    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("sujet") Groupe groupe);
}