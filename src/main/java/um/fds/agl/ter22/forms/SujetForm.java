package um.fds.agl.ter22.forms;

public class SujetForm {

    private String titre;
    private long id;
    private String enseignantResponsable;

    private String encadrant;

    public SujetForm(long id, String titre, String enseignantResponsable, String encadrant) {
        this.titre = titre;
        this.enseignantResponsable = enseignantResponsable;
        this.id = id;
        this.encadrant=encadrant;
    }

    public SujetForm() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getEnseignantResponsable() {
        return enseignantResponsable;
    }

    public void setEnseignantResponsable(String enseignantResponsable) {this.enseignantResponsable = enseignantResponsable;}

    public String getEncadrant(){return encadrant;}

    public void setEncadrant(String encadrant){this.encadrant=encadrant;}
}

