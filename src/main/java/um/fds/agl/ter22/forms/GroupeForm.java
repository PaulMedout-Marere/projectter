package um.fds.agl.ter22.forms;

public class GroupeForm {
    private String nom;
    private long id;

    public GroupeForm(String nom, long id) {
        this.nom = nom;
        this.id = id;
    }
    public  GroupeForm(){}

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
