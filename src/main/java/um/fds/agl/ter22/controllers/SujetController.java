package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Sujet;
import um.fds.agl.ter22.forms.SujetForm;
import um.fds.agl.ter22.services.SujetService;

@Controller
public class SujetController implements ErrorController {

    @Autowired
    private SujetService sujetService;

    @GetMapping("/listSujets")
    public Iterable<Sujet> getSujets(Model model) {
        model.addAttribute("sujet", sujetService.getSujets());
        return sujetService.getSujets();
    }

    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = { "/addSujet" })
    public String showAddSujetPage(Model model) {
        SujetForm sujetForm = new SujetForm();
        model.addAttribute("sujetForm", sujetForm);

        return "addSujet";
    }

    @PostMapping(value = { "/addSujet"})
    public String addSujet(Model model, @ModelAttribute("SujetForm") SujetForm sujetForm) {
        Sujet sujet;
        if(sujetService.findById(sujetForm.getId()).isPresent()){
            sujet = sujetService.findById(sujetForm.getId()).get();
            sujet.setEnseignantResponsable(sujetForm.getEnseignantResponsable());
            sujet.setTitre(sujetForm.getTitre());
            sujet.setEncadrant(sujetForm.getEncadrant());
        } else {
            sujet = new Sujet(sujetForm.getEnseignantResponsable(),sujetForm.getTitre(), sujetForm.getEncadrant());
        }
        sujetService.saveSujet(sujet);
        return "redirect:/listSujets";
    }

    @GetMapping(value = {"/showSujetUpdateForm/{id}"})
    public String showSujetUpdateForm(Model model, @PathVariable(value = "id") long id){

        SujetForm sujetForm = new SujetForm(id, sujetService.findById(id).get().getTitre(), sujetService.findById(id).get().getEnseignantResponsable(), sujetService.findById(id).get().getEncadrant());
        model.addAttribute("sujetForm", sujetForm);
        return "updateSujet";
    }

    @GetMapping(value = {"/groupSujet/{id}"})
    public String groupSujet(Model model, @PathVariable(value = "id") long id){

        //SujetForm sujetForm = new SujetForm(id, sujetService.findById(id).get().getTitre(), sujetService.findById(id).get().getEnseignantResponsable(), sujetService.findById(id).get().getEncadrant());
        //model.addAttribute("sujetForm", sujetForm);
        return "groupSujet";
    }

    @GetMapping(value = {"/deleteSujet/{id}"})
    public String deleteSujet(Model model, @PathVariable(value = "id") long id){
        sujetService.deleteSujet(id);
        return "redirect:/listSujets";
    }

}
